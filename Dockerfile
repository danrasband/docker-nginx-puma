FROM roonglit/rbenv
MAINTAINER Roonglit Chareonsupkul <roonglit@gmail.com>

# Ignore APT warnings about not having a TTY
ENV DEBIAN_FRONTEND noninteractive

# Ensure UTF-8 locale
RUN echo "LANG=\"en_US.UTF-8\"" > /etc/default/locale
RUN locale-gen en_GB.UTF-8
RUN dpkg-reconfigure locales

# add apt-get repository source
ADD sources.list /etc/apt/sources.list
RUN apt-get update

# install dependencies
RUN apt-get install -qy software-properties-common supervisor --fix-missing

# redis server, using elasticache now
# RUN apt-get install -qy redis-server --fix-missing

# install nginx
RUN add-apt-repository -y ppa:nginx/stable
RUN apt-get update
RUN apt-get install -qy nginx
RUN echo "\ndaemon off;" >> /etc/nginx/nginx.conf

# add default nginx config
ADD nginx-sites.conf /etc/nginx/sites-enabled/default

# set up app folder
WORKDIR /app

# add default puma config
ADD puma.rb /app/config/puma.rb

ENV RAILS_ENV production

# add default supervisor config
ADD supervisord.conf /etc/supervisor/conf.d/supervisord.conf 

# add start-app script for Puma
ADD start-app.sh /usr/bin/start-app
RUN chmod +x /usr/bin/start-app

EXPOSE 80
