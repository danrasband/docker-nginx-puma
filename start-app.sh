#!/bin/bash

cd /app
bundle exec rake db:migrate
bundle exec puma -e production -d
